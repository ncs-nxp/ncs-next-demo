package sg.ncs.next.demo.product.web.config.thread;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.interceptor.SimpleAsyncUncaughtExceptionHandler;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;

public class AsyncExceptionHandler extends SimpleAsyncUncaughtExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(AsyncExceptionHandler.class);



    @Override
    public void handleUncaughtException(Throwable ex, Method method, Object... params) {

        StringWriter sw = new StringWriter();
        ex.printStackTrace(new PrintWriter(sw,true));
        logger.error(ex.getClass().getName() + " " + sw.toString(), this.getClass());
    }
}
