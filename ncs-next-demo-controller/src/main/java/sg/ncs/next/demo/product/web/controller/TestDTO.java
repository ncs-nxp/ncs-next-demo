package sg.ncs.next.demo.product.web.controller;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class TestDTO {

    @ApiModelProperty(value = "name")
    private String name;

    @ApiModelProperty(value = "Array parameter")
    private List<String> typeList;

    @ApiModelProperty(value = "Date parameter yyyy-MM-dd")
    private LocalDate date;

    @ApiModelProperty(value = "Time parameter yyyy-MM-dd HH:mm:ss")
    private LocalDateTime time;

    @ApiModelProperty(value = "order number", required = true)
    private String orderNo;
}
