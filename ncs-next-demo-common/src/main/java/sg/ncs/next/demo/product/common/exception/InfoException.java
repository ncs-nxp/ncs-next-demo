package sg.ncs.next.demo.product.common.exception;



/**
* Business exception
*
*/
public class InfoException extends RuntimeException{
    private Integer msgCode;

    public InfoException(String message) {
        super(message);
    }

    public InfoException(Integer msgCode,String message) {
        super(message);
        this.msgCode = msgCode;
    }

    public Integer getMsgCode() {
        return msgCode;
    }

    public void setMsgCode(Integer msgCode) {
        this.msgCode = msgCode;
    }
}
