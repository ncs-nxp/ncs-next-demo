package sg.ncs.next.demo.product.web;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;


@EnableScheduling
@EnableFeignClients("sg.ncs.next.demo.product.*")
@SpringBootApplication(scanBasePackages = "sg.ncs.next.demo.product.*")
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }


}
