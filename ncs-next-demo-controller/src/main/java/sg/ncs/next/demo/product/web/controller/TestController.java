package sg.ncs.next.demo.product.web.controller;



import sg.ncs.next.demo.product.common.basic.ResultVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;




@Api(value="TestController",description="Debug, please do not adjust")
@RestController
@RequestMapping("/test")
public class TestController {
    private static final Logger logger = LoggerFactory.getLogger(TestController.class);

    @ApiOperation(value = "test1")
    @GetMapping("/test1")
    public ResultVO test1(){
        ResultVO resultVO=new ResultVO();
        resultVO.setCode(1);
        return resultVO;
    }

}
